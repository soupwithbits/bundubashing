﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Boundaries : MonoBehaviour
{
	public float BoundaryMin, BoundaryMax;
	private Transform _carTransform;
	private Image _image;

    void Start()
    {
		_image = GetComponent<Image>();
		_carTransform = GameObject.FindWithTag("Player").transform;
    }
	
    void Update()
    {
    	var distanceFromMiddle = Vector3.Distance(Vector3.zero, _carTransform.position);
		var color = _image.color;
		color.a = UtilityFunctions.Map(distanceFromMiddle, BoundaryMin, BoundaryMax, 0f, 1f);
		_image.color = color;

		if (color.a > 0.98f)
			print("end game");
    }
}
