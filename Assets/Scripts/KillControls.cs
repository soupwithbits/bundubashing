﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KillControls : MonoBehaviour {

	[SerializeField]
	float TimeBeforeFade = 3;
	[SerializeField]
	float FadeTime = 3;

	void Start () 
	{
		foreach (var item in GetComponentsInChildren<MaskableGraphic>())
		{
			GoTweenConfig config = new GoTweenConfig();
			Color color = item.color;
			color.a = 0;
			config.colorProp("color", color);
			config.setDelay(TimeBeforeFade);
			Go.to(item, FadeTime, config);
		}
	}
}
