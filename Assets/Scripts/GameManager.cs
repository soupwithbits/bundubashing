using UnityEngine;
using System.Collections;
using InControl;

public class GameManager : MonoBehaviour
{
    public int TargetFrameRate = -1;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
			SoundManager.PlayMusic(gameObject);
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (this != _instance)
                Destroy(gameObject);
        }

        Application.targetFrameRate = TargetFrameRate;
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            Application.LoadLevel(Application.loadedLevel);

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}
