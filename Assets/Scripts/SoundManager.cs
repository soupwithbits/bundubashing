﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public float RevValue;
	public float fCarHealth;
	public float ComboC;

    private CarController _car;

    void Start()
    {
		AkSoundEngine.SetRTPCValue ("CarHealth", fCarHealth, this.gameObject);
        AkSoundEngine.PostEvent("Play_birds", gameObject);
        AkSoundEngine.PostEvent("Play_Engine", gameObject);
        _car = FindObjectOfType<CarController>();

        // if you want the health use _car.Health


        // Drifting
        // _car.IsDrifting - boolean
        // _car.DriftingIntensity (0 - 1)


    }
	
    void Update()
    {
        AkSoundEngine.SetRTPCValue("Rev", RevValue * 100);
		fCarHealth = _car.Health;
		AkSoundEngine.SetRTPCValue ("CarHealth", fCarHealth, this.gameObject);

		//car drift sound
		if (_car.IsDrifting == true) 
		{
			AkSoundEngine.PostEvent("Play_Squeals",this.gameObject);
		}

		ComboC = ComboManager.instance.comboCount;
		AkSoundEngine.SetRTPCValue ("ComboCount", ComboC, this.gameObject);

    }

    public static void PlayWwiseStuff(int param)
    {
    	//make a function like this we'll be able to call it from anywhere
    	//the (int param) is the parameter that you'd need in the function 
    }

	public static void PlayCarDeath()
	{
		AkSoundEngine.PostEvent("Play_engine_die", Camera.main.gameObject);
		AkSoundEngine.SetState ("DynamicMix", "Death");
		AkSoundEngine.SetSwitch ("Music_Switch", "Nothing", GameManager.Instance.gameObject);

	}

	//Starts the Music
	public static void PlayMusic(GameObject GameMan)
	{
		AkSoundEngine.PostEvent ("Play_Music", GameMan);
	}

	public static void DMixTitle()
	{
		AkSoundEngine.SetState ("DynamicMix", "TitleScreen");
	}

	//Switching functionality for music

	//Call for metal
	public static void SwitchToMetal()
	{
		AkSoundEngine.SetSwitch ("Music_Switch", "Metal", GameManager.Instance.gameObject);
		AkSoundEngine.SetState ("DynamicMix", "Combo");
	}
	//call for safari music
	public static void SwitchToSafari()
	{
		AkSoundEngine.SetSwitch ("Music_Switch", "Safari", GameManager.Instance.gameObject);
		AkSoundEngine.SetState ("DynamicMix", "GamePlay");
	}

	// plays zebra sound -- only once per code call (This can be changed on my end) 
	public static void PlayZebraIdle(GameObject zebra)
	{
		AkSoundEngine.PostEvent ("Play_ZebraIdle", zebra);
	}
	// plays Boar sound -- only once per code call (This can be changed on my end) 
	public static void PlayBoarIdle(GameObject boar)
	{
		AkSoundEngine.PostEvent ("Play_boar", boar);
	}
	// plays Elephant Trumpeting sound -- only once per code call (This can be changed on my end) 
	public static void PlayElephantIdle(GameObject elephant)
	{
		AkSoundEngine.PostEvent ("Play_EleIdle", elephant);
	}
	// plays Elephant Charging sound -- only once per code call (This can be changed on my end) 
	public static void PlayElephantCharge(GameObject elephant)
	{
		AkSoundEngine.PostEvent ("Play_EleCharge", elephant);
	}

	// plays elephant tusk collision sound
	public static void PlayTuskHit(GameObject elephant)
	{
		AkSoundEngine.PostEvent ("Play_TuskHit", elephant);
	}

}
