﻿using UnityEngine;
using System.Collections;

public class ChargingAnimalController : BoidFlocking 
{
	[SerializeField]
	float ChargeSpeed = 500;
	[SerializeField]
	float ChargeInitDistance = 750;
	[HideInInspector]
	public bool Charging = false;

	public void ChargePlayer()
	{
		isFlocking = false;
		Charging = true;

		Vector3 v = player.position - transform.position;
		var angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg + 90;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		Go.killAllTweensWithTarget(transform);
		GoTweenConfig config = new GoTweenConfig();
		config.position(player.position);
		config.setEaseType(GoEaseType.SineIn);
		config.onComplete(_ => { isFlocking = true; Charging = false; });
		float time = Vector2.Distance(player.position, transform.position) / ChargeSpeed;

		Go.to(transform, time, config);
		SoundManager.PlayElephantCharge(gameObject);
	}
}
