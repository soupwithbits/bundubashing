﻿using UnityEngine;
using System.Collections;

public class TrailManager : MonoBehaviour 
{
	[SerializeField]
	TrailRenderer LeftTire;
	[SerializeField]
	TrailRenderer RightTire;
	[SerializeField]
	TrailRenderer LeftTireBlood;
	[SerializeField]
	TrailRenderer RightTireBlood;
	[SerializeField]
	TrailRenderer RearLeftTireBlood;
	[SerializeField]
	TrailRenderer RearRightTireBlood;
	[SerializeField]
	float BloodyTireness = 2.0f;

	private float left = 0, right = 0, rearRight = 0, rearLeft;

	void Start()
	{
		TireCollision.OnTireInBlood += OnTireInBlood;
		LeftTire.sortingLayerName = "Blood";
		LeftTire.sortingOrder = -1;
		RightTire.sortingLayerName = "Blood";
		RightTire.sortingOrder = -1;
		LeftTireBlood.sortingLayerName = "Blood";
		LeftTireBlood.sortingOrder = 1;
		RightTireBlood.sortingLayerName = "Blood";
		RightTireBlood.sortingOrder = 1;
		RearLeftTireBlood.sortingLayerName = "Blood";
		RearLeftTireBlood.sortingOrder = 1;
		RearRightTireBlood.sortingLayerName = "Blood";
		RearRightTireBlood.sortingOrder = 1;
	}
	void OnDestroy()
	{
		TireCollision.OnTireInBlood += OnTireInBlood;
	}

	void OnTireInBlood(TireCollision.Tire tirePosition)
	{
		switch (tirePosition)
		{
			case TireCollision.Tire.FrontLeft:
				left = BloodyTireness;
				break;
			case TireCollision.Tire.FrontRight:
				right = BloodyTireness;
				break;
			case TireCollision.Tire.RearLeft:
				rearLeft = BloodyTireness;
				break;
			case TireCollision.Tire.RearRight:
				rearRight = BloodyTireness;
				break;
		}
	}

	void Update()
	{
		left = Mathf.Clamp(left -= 0.025f, 0, BloodyTireness);
		right = Mathf.Clamp(right -= 0.025f, 0, BloodyTireness);
		rearLeft = Mathf.Clamp(left -= 0.025f, 0, BloodyTireness);
		rearRight = Mathf.Clamp(right -= 0.025f, 0, BloodyTireness);

		LeftTireBlood.time = left;
		RightTireBlood.time = right;
		RearLeftTireBlood.time = rearLeft;
		RearRightTireBlood.time = rearRight;
	}
}
