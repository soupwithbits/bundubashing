﻿using UnityEngine;
using System.Collections;

public class DamagingObstacle : MonoBehaviour 
{
	public delegate void OnHitObstacleDelegate(DamagingObstacle animal);
	public static OnHitObstacleDelegate OnHitObstacle;

	public int DamageOnHit = 10;

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.CompareTag("Player"))
		{
			if (OnHitObstacle != null)
			{
				OnHitObstacle(this);
				CameraBehaviour.Instance.ShakeScreen(10f, 10f, 0.2f);
				if (transform.parent.name == "Elephant")
				{
					SoundManager.PlayTuskHit(gameObject);
				}
				else
				{
					AkSoundEngine.PostEvent("Play_Treehits", gameObject);
				}
			}
		}
	}
}
