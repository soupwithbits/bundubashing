﻿using UnityEngine;
using System.Collections;

public class StatManager : MonoBehaviour {

	public static StatManager instance;

	[HideInInspector]
	public float LitersOfBlood = 0.0f;
	[HideInInspector]
	public float KGOfIvory = 0.0f;
	[HideInInspector]
	public int BoarHooves = 0;
	[HideInInspector]
	public int ZebraSteaks = 0;

	// Use this for initialization
	void Start () 
	{
		AnimalController.OnAnimalKilled += OnAnimalKilled;
		instance = this;
	}

	void OnDestroy()
	{
		AnimalController.OnAnimalKilled -= OnAnimalKilled;
	}
	
	void OnAnimalKilled(AnimalController animal)
	{
		if (animal.name.Contains("Boar"))
		{
			BoarHooves += Random.Range(1, 5);
			LitersOfBlood += Random.Range(5.0f, 15.0f);
		}
		if (animal.name.Contains("Zebra"))
		{
			ZebraSteaks += Random.Range(5, 10);
			LitersOfBlood += Random.Range(30.0f, 50.0f);
		}
		else
		{
			KGOfIvory += Random.Range(1.0f, 50.0f);
			LitersOfBlood += Random.Range(150.0f, 250.0f);
		}
	}

	// Update is called once per frame
	void Save ()
	{
		PlayerPrefs.SetFloat("LitersOfBlood",LitersOfBlood);
		PlayerPrefs.SetFloat("KGOfIvory",KGOfIvory);
		PlayerPrefs.SetInt("BoarHooves",BoarHooves);
		PlayerPrefs.SetInt("ZebraSteaks", ZebraSteaks);
		PlayerPrefs.Save();
	}

	void Load()
	{
		LitersOfBlood = PlayerPrefs.GetFloat("LitersOfBlood");
		KGOfIvory = PlayerPrefs.GetFloat("KGOfIvory");
		BoarHooves = PlayerPrefs.GetInt("BoarHooves");
		ZebraSteaks = PlayerPrefs.GetInt("ZebraSteaks");
	}
}
