﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BloodManager : MonoBehaviour 
{
	[SerializeField]
	GameObject Blood;

	public static BloodManager instance;

	void Start()
	{
		instance = this;
	}
	public void CreateSplatter(Vector3 position, float scaleBy = 1)
	{
		GameObject g = new GameObject("Blood" + Time.time);
		g.transform.SetParent(transform);
		g.transform.position = position;
		for (int i = 0; i < 50; i++)
		{
			Vector3 pos, scale;
			float p;
			if (i < 5)
			{
				p = 15f;
				scale = Vector3.one * Random.Range(0.4f, 0.75f);
			}
			else if (i < 15)
			{
				p = 25f;
				scale = Vector3.one * Random.Range(0.25f, 0.4f);
			}
			else if (i < 30)
			{
				p = 35f;
				scale = Vector3.one * Random.Range(0.125f, 0.25f);
			}
			else
			{
				p = 45f;
				scale = Vector3.one * Random.Range(0f, 0.125f);
			}
			pos = position + new Vector3(Random.Range(-p, p), Random.Range(-p, p));
			var b = Instantiate(Blood, pos, Quaternion.identity) as GameObject;
			b.transform.localScale = scale;
			b.transform.SetParent(g.transform);
		}
		g.transform.localScale *= scaleBy;
		var collider = g.AddComponent<CircleCollider2D>();
		collider.isTrigger = true;
		collider.radius = 35;
		g.tag = "Blood";
	}

	public void CreateSplatter(Vector3 position, Vector3 SplatterToPoint)
	{
		for (int i = 0; i < 50; i++)
		{
			Vector3 pos, scale;
			float circleSize;
			float percBetweenPoints;
			if (i < 8)
			{
				circleSize = 1.8f;
				percBetweenPoints = 0;
				scale = Vector3.one * Random.Range(3f, 5f);
			}
			else if (i < 15)
			{
				percBetweenPoints = 0.3f;
				circleSize = 1.25f;
				scale = Vector3.one * Random.Range(2.5f, 3f);
			}
			else if (i < 30)
			{
				percBetweenPoints = 0.5f;
				circleSize = 0.5f;
				scale = Vector3.one * Random.Range(1f, 2f);
			}
			else
			{
				percBetweenPoints = 9f;
				circleSize = 0.25f;
				scale = Vector3.one * Random.Range(0f, 1f);
			}
			pos = position + SplatterToPoint*percBetweenPoints + new Vector3(Random.Range(-circleSize, circleSize), Random.Range(-circleSize, circleSize));
			var b = Instantiate(Blood, pos, Quaternion.identity) as GameObject;
			b.transform.localScale = scale;
		}
	}
}
