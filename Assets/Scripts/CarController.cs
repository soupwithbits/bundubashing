using UnityEngine;
using InControl;
using System.Collections;

public class CarController : MonoBehaviour
{
    public float MoveForce = 400f, 
        TurnSpeed = 100f, 
        MaxSpeed = 1000f, 
        MaxAngularSpeed = 100f,
        DriftSpeed = 400f,
        DriftForce = 500f;


    public bool IsDrifting;
    public float CurrentSpeed;
    public float DriftingIntensity;

    private Rigidbody2D _rigidbody2D;
    private SoundManager _soundManager;
    private ParticleSystem vehicleDamage;
    private bool _firstKill = true;

    private float health = 50;
    private bool isWasted;
    public  float Health
    {
        get { return health; }
        set
        {
            health = value;
            vehicleDamage.emissionRate = 50 - health;
            if (health <= 0 && !isWasted)
            {
                LevelManager.instance.LevelFailed();
                //AkSoundEngine.PostEvent("Play_engine_die", Camera.main.gameObject);
				SoundManager.PlayCarDeath();
                isWasted = true;
            }
        }
    }

    void Start()
    {
        AnimalController.OnAnimalKilled += OnAnimalKilled;
        DamagingObstacle.OnHitObstacle += OnObstacleHit;
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _soundManager = FindObjectOfType<SoundManager>();
        vehicleDamage = transform.Find("VehicleDamage").GetComponent<ParticleSystem>();
    }

    void OnDestroy()
    {
        AnimalController.OnAnimalKilled -= OnAnimalKilled;
        DamagingObstacle.OnHitObstacle -= OnObstacleHit;
    }

    void FixedUpdate()
    {
        var inputDevice = InputManager.ActiveDevice;

        var accelerating = inputDevice.RightTrigger;
        var reversing = inputDevice.LeftTrigger;

        CurrentSpeed = Vector2.Dot(transform.up, _rigidbody2D.velocity);
        _soundManager.RevValue = Mathf.Abs(CurrentSpeed / MaxSpeed);
        
        if (Health > 0 && accelerating)
        {
            if (CurrentSpeed < MaxSpeed)
                _rigidbody2D.AddForce(transform.up * MoveForce);
        }

        if (Health > 0 && reversing)
        {
            var m = -MaxSpeed * 0.4f;
            if (CurrentSpeed > m)
                _rigidbody2D.AddForce(transform.up * -MoveForce);
        }

        _rigidbody2D.drag = (!accelerating && !reversing) ? 0.5f : 0f;

        var turn = inputDevice.LeftStickX;
        var s = Mathf.Abs(CurrentSpeed);
        var x = UtilityFunctions.Map(s, 0f, MaxSpeed, 0f, 1f);
        var r = CurrentSpeed > 0f ? 1f : -1f;
        
        if (turn.Value < 0f)
        {
            if (_rigidbody2D.angularVelocity < MaxAngularSpeed)
                _rigidbody2D.AddTorque(TurnSpeed * x * r);
        }

        if (turn.Value > 0f)
        {
            if (_rigidbody2D.angularVelocity > -MaxAngularSpeed)
                _rigidbody2D.AddTorque(-TurnSpeed * x * r);
        }

        if (turn.Value == 0f)
            _rigidbody2D.angularVelocity = 0f;

        var lateralVelocity = Vector2.Dot(transform.right, _rigidbody2D.velocity) * transform.right;

        IsDrifting = lateralVelocity.magnitude > DriftSpeed;
        if (IsDrifting)
            _rigidbody2D.AddForce(lateralVelocity.normalized * DriftForce);
            
        DriftingIntensity = IsDrifting ? DriftSpeed / lateralVelocity.magnitude : 1f;


        _rigidbody2D.velocity = _rigidbody2D.velocity - (Vector2)lateralVelocity * DriftingIntensity;
    }

    void OnAnimalKilled(AnimalController a)
    {
        if (a == null)
            return;

        if (_firstKill)
        {
            SoundManager.SwitchToMetal();
            _firstKill = false;
        }

        CameraBehaviour.Instance.ShakeScreenInc(10f * a.BloodScale, 10f * a.BloodScale, 0.2f * a.BloodScale);
    }

    void OnObstacleHit(DamagingObstacle obstacle)
    {
        Health -= obstacle.DamageOnHit;
    }
}
