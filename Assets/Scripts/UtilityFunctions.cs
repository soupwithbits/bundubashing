﻿using UnityEngine;

public static class UtilityFunctions
{
	public static float Map(float value, float initialMin, float initialMax, float destinationMin, float destinationMax)
	{
		var t = (value - initialMin) / (initialMax - initialMin);
		return Mathf.Lerp(destinationMin, destinationMax, t);
	}
}
