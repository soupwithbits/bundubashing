﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour
{
    void Start()
    {
        var sprite = GetComponent<SpriteRenderer>();
        var c = sprite.color;
        c.a = 0f;
        var config = new GoTweenConfig();
        config.colorProp("color", c);
        config.onComplete(_ => StartCoroutine("FadeIn"));

        Go.to(sprite, 2f, config);
    }
	
    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(3f);

        var sprite = GetComponent<SpriteRenderer>();
        var c = sprite.color;
        c.a = 1f;
        var config = new GoTweenConfig();
        config.colorProp("color", c);
        config.onComplete(_ =>
            {
                Application.LoadLevel(1);
            });
        Go.to(sprite, 2f, config);
    }
}
