﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour 
{
	[HideInInspector]
	public int Score;
	private Text text;

	public static ScoreManager instance;

	// Use this for initialization
	void Start () 
	{
		instance = this;
		AnimalController.OnAnimalKilled += OnAnimalKilled;
		text = GameObject.Find("ScoreText").GetComponent<Text>();
	}
	
	void OnDestroy()
	{
		AnimalController.OnAnimalKilled -= OnAnimalKilled;
	}

	void OnAnimalKilled(AnimalController animal)
	{
		if (LevelManager.instance.GameOver)
		{
			return;
		}
		Score += (int)(animal.BloodScale * (ComboManager.instance.comboCount+1)  * (ComboManager.instance.DriftCombo+1));
		text.text = "Score: " + Score;
		text.fontSize = 100;

		GoTween t = null;
		GoTweenConfig config = new GoTweenConfig();
		config.intProp("fontSize", 150);
		config.setEaseType(GoEaseType.SineInOut);
		config.onComplete(_ => { text.fontSize = 100; t.reverse(); });
	
		t = Go.to(text, 0.5f, config);
	}
}
