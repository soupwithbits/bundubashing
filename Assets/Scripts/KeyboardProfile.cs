using System;
using System.Collections;
using UnityEngine;
using InControl;

// This custom profile is enabled by adding it to the Custom Profiles list
// on the InControlManager component, or you can attach it yourself like so:
// InputManager.AttachDevice( new UnityInputDevice( "KeyboardAndMouseProfile" ) );
//
public class KeyboardProfile : UnityInputDeviceProfile
{
    public KeyboardProfile()
    {
        Name = "Keyboard/Mouse";
        Meta = "A keyboard and mouse combination profile appropriate for FPS.";

        // This profile only works on desktops.
        SupportedPlatforms = new[]
        {
            "Windows",
            "Mac",
            "Linux"
        };

        Sensitivity = 1.0f;
        LowerDeadZone = 0.0f;
        UpperDeadZone = 1.0f;

        ButtonMappings = new[]
        {
            new InputControlMapping
            {
                Handle = "Accelerate",
                Target = InputControlType.RightTrigger,
                // KeyCodeAxis splits the two KeyCodes over an axis. The first is negative, the second positive.
                Source = KeyCodeButton(KeyCode.W, KeyCode.UpArrow)
            },
            new InputControlMapping
            {
                Handle = "BrakeReverse",
                Target = InputControlType.LeftTrigger,
                // KeyCodeAxis splits the two KeyCodes over an axis. The first is negative, the second positive.
                Source = KeyCodeButton(KeyCode.S, KeyCode.DownArrow)
            }
        };

        AnalogMappings = new[]
        {
            new InputControlMapping
            {
                Handle = "Move X",
                Target = InputControlType.LeftStickX,
                // KeyCodeAxis splits the two KeyCodes over an axis. The first is negative, the second positive.
                Source = KeyCodeAxis(KeyCode.A, KeyCode.D)
            },
            new InputControlMapping
            {
                Handle = "Move X Alternate",
                Target = InputControlType.LeftStickX,
                Source = KeyCodeAxis(KeyCode.LeftArrow, KeyCode.RightArrow)
            }
        };
    }
}

