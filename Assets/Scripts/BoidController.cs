﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoidController : MonoBehaviour
{
	public float minVelocity = 5;
	public float maxVelocity = 20;
	public float randomness = 1;
	public int flockSize = 20;
	public GameObject prefab;
	public GameObject chasee;

	[HideInInspector]
	public int ScoreFromFlock;
	[HideInInspector]
	public Vector3 flockCenter;
	[HideInInspector]
	public Vector3 flockVelocity;
	[HideInInspector]
	public List<BoidFlocking> boids;

	private Transform player;

	void Start()
	{
		boids = new List<BoidFlocking>();
		for (var i = 0; i < flockSize; i++)
		{
			var collider2D = GetComponent<Collider2D>();
			Vector3 position = new Vector3(
				Random.value * collider2D.bounds.size.x,
				Random.value * collider2D.bounds.size.y) - collider2D.bounds.extents;

			GameObject boid = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
			boid.transform.parent = transform;
			boid.transform.localPosition = position;
			boid.GetComponent<BoidFlocking>().SetController(this);
			boids.Add(boid.GetComponent<BoidFlocking>());
		}
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update()
	{
		Vector3 theCenter = Vector3.zero;
		Vector2 theVelocity = Vector2.zero;

		foreach (BoidFlocking boid in boids)
		{
			theCenter += boid.transform.localPosition;
			theVelocity += boid.rigidbody2D.velocity;
		}

		flockCenter = theCenter / (flockSize);
		flockVelocity = theVelocity / (flockSize);
	}

	public void SetFrigtenedState()
	{
		maxVelocity = 100;
		randomness = 200;
	}
	public void SetNormalState()
	{
		maxVelocity = 10;
		randomness = 10;
	}

	public void RemoveAnimal(BoidFlocking flocking)
	{
		boids.Remove(flocking);

		//release all the charges
		if (flocking is ChargingAnimalController)
		{
			foreach (var item in boids)
			{
				(item as ChargingAnimalController).ChargePlayer();
			}
		}
	}
}