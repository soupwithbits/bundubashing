﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class HerdSoundController : MonoBehaviour {

	void Start () 
	{
		InvokeRepeating("PlayRandomIdle", 2, 3);
	}
	
	void PlayRandomIdle () 
	{
		int r = Random.Range(0, 3);
		switch (r)
		{
			case 0:	
				var boars =  GameObject.FindGameObjectsWithTag("Boar");
				SoundManager.PlayBoarIdle(boars[Random.Range(0,boars.Length)]);
				break;
			case 1:
				var zebras = GameObject.FindGameObjectsWithTag("Zebra");
				SoundManager.PlayZebraIdle(zebras[Random.Range(0, zebras.Length)]);
				break;
			case 2:
				var elephants = GameObject.FindGameObjectsWithTag("Elephant");
				SoundManager.PlayElephantIdle(elephants[Random.Range(0, elephants.Length)]);
				break;
		}
	}
}
