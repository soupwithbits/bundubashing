﻿using UnityEngine;
using System.Collections;

public class AnimalController : MonoBehaviour 
{
	public delegate void OnAnimalKilledDelegate(AnimalController animal);
	public static OnAnimalKilledDelegate OnAnimalKilled;

	[SerializeField]
	public float BloodScale = 1;
	private BoidFlocking flocking;

	[SerializeField]
	GameObject Splat;

	void Start()
	{
		flocking = GetComponent<BoidFlocking>();
	}

	void OnTriggerEnter2D(Collider2D coll) 
	{
		if (coll.CompareTag("Player"))
		{
			BloodManager.instance.CreateSplatter(transform.position, BloodScale);
			if (OnAnimalKilled != null)
			{
				OnAnimalKilled(this);
			}
			flocking.Controller.RemoveAnimal(flocking);
			if (Splat != null)
			{
				var splat = Instantiate(Splat, transform.position, Quaternion.identity) as GameObject;
				splat.transform.eulerAngles = new Vector3(1, 1, Random.Range(0, 360));
			}
			Go.killAllTweensWithTarget(transform);
			Destroy(gameObject);

			AkSoundEngine.PostEvent("Play_Death", gameObject);
		}
	}
}