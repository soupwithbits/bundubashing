﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HerdIndicator : MonoBehaviour 
{
	[SerializeField]
	GameObject WarthogArrow;
	[SerializeField]
	GameObject ZebraArrow;
	[SerializeField]
	GameObject ElephantArrow;

	private List<Herd> herds;

	private struct Herd
	{
		public BoidController herd;
		public GameObject arrow;

		public Herd(BoidController h, GameObject a)
		{
			herd = h;
			arrow = a;
		}
	}

	void Start () 
	{
		var h = FindObjectsOfType<BoidController>();
		herds = new List<Herd>();
		for (int i = 0; i < h.Length; i++)
		{
			switch (h[i].prefab.name)
			{
				case "Elephant":
					herds.Add(new Herd(h[i], Instantiate(ElephantArrow) as GameObject));
					break;
				case "Boar":
					herds.Add(new Herd(h[i], Instantiate(WarthogArrow) as GameObject));
					break;
				case "Zebra":
					herds.Add(new Herd(h[i], Instantiate(ZebraArrow) as GameObject));
					break;
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (herds.Count == 0)
		{
			LevelManager.instance.LevelComplete();
		}

		int toDelete = -1;
		for (int i = 0; i < herds.Count; i++)
		{
			if (herds[i].herd.boids.Count == 0)
			{
				toDelete = i;
				break;
			}

			var playerPosition = GameObject.FindObjectOfType<CarController>().transform.position;
			herds[i].arrow.transform.position = playerPosition;

			Vector3 v = herds[i].herd.transform.position - playerPosition;
			var angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
			herds[i].arrow.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}

		if (toDelete != -1)
		{
			Destroy(herds[toDelete].arrow);
			herds.RemoveAt(toDelete);
		}
	}
}
