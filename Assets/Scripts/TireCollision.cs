﻿using UnityEngine;
using System.Collections;

public class TireCollision : MonoBehaviour 
{
	public enum Tire
	{
		FrontLeft, FrontRight, RearLeft, RearRight
	}
	public delegate void OnTireInBloodDelegate(Tire tirePostion);
	public static OnTireInBloodDelegate OnTireInBlood;

	[SerializeField]
	Tire TirePosition;

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.CompareTag("Blood"))
		{
			if (OnTireInBlood != null)
			{
				OnTireInBlood(TirePosition);
			}
		}
	}
}
