﻿using UnityEngine;
using System.Collections;

public class DigginAnimal : BoidFlocking
{
	public enum BoarState
	{
		DiggingDown, DiggingUp, Up, Underground
	}

	private BoarState state = BoarState.Up;

	[SerializeField]
	float FloatDigTime = 5;
	[SerializeField]
	float DigInitDistance = 750;
	SpriteRenderer sprite;
	[SerializeField]
	Sprite DiggingSprite;
	[SerializeField]
	Sprite NormalSprite;
	[SerializeField]
	Sprite DiggingUp;

	ParticleSystem[] particleSystems;

	void OnDestroy()
	{
		Go.killAllTweensWithTarget(sprite);
	}
	private void Dig()
	{
		state = BoarState.DiggingDown;
		Go.killAllTweensWithTarget(sprite);
		isFlocking = false;
		TurnOnParticles();

		if (sprite == null)
		{
			sprite = GetComponentInChildren<SpriteRenderer>();
		}
		sprite.sprite = DiggingSprite;
		rigidbody2D.isKinematic = true;

		GoTweenConfig config = new GoTweenConfig();
		config.colorProp("color", new Color(1, 1, 1, 0));
		config.onComplete(_ => OnDugDown());

		float tweenTime = (1 - sprite.color.a) * FloatDigTime;
		Go.to(sprite, FloatDigTime, config);
	}

	void OnDugDown()
	{ 
		GetComponent<BoxCollider2D>().enabled = false; 
		TurnOffParticles();
		state = BoarState.Underground;
	}

	private void Undig()
	{
		state = BoarState.Up;

		TurnOnParticles();
		sprite.sprite = DiggingUp;
		sprite.color = new Color(1, 1, 1, 0.2f);
		GetComponent<BoxCollider2D>().enabled = true;
		GoTweenConfig config = new GoTweenConfig();
		config.colorProp("color", new Color(1, 1, 1, 1));
		config.onComplete(_ => OnDugOut());

		Go.to(sprite, FloatDigTime / 2, config);
	}

	void OnDugOut()
	{
		isFlocking = true; 
		sprite.sprite = NormalSprite; 
		TurnOffParticles();
		state = BoarState.Up;
	}

	void Update()
	{
		if (state ==  BoarState.DiggingUp || state == BoarState.Underground)
		{
			if (Vector2.Distance(player.position, transform.position) > DigInitDistance)
			{
				Undig();
			}
		}
		else if (state == BoarState.Up)
		{
			if (Vector2.Distance(player.position, transform.position) < DigInitDistance)
			{
				Dig();
			}
		}
	}

	bool particlesPlaying = false;
	void TurnOnParticles()
	{
		if (particlesPlaying)
			return;

		if (particleSystems == null)
			particleSystems = GetComponentsInChildren<ParticleSystem>();

		foreach (var item in particleSystems)
		{
			item.Play();
		}
		particlesPlaying = true;
	}

	void TurnOffParticles()
	{
		if (!particlesPlaying)
			return;
		foreach (var item in particleSystems)
		{
			item.Stop();
		}
		particlesPlaying = false;
	}
}
