﻿using UnityEngine;
using System.Collections;

public class BoidFlocking : MonoBehaviour
{
	[HideInInspector]
	public BoidController Controller;
	private bool inited = false;
	[HideInInspector]
	public bool isFlocking = true;
	[HideInInspector]
	public Rigidbody2D rigidbody2D;
	protected Transform player;

	void Awake()
	{
		rigidbody2D = GetComponent<Rigidbody2D>();
	}
	void Start()
	{
		StartCoroutine("BoidSteering");
		player = GameObject.FindGameObjectWithTag("Player").transform;

		var animator = GetComponent<Animator>();
		if (animator != null)
		{
			animator.Play(animator.GetCurrentAnimatorStateInfo(0).shortNameHash, 0, Random.Range(0f, 1f));
		}
	}

	IEnumerator BoidSteering()
	{
		while (true)
		{
			if (inited)
			{
				if (isFlocking)
				{
					rigidbody2D.velocity += Calc() * Time.deltaTime;

					// enforce minimum and maximum speeds for the boids
					float speed = rigidbody2D.velocity.magnitude;
					if (speed > Controller.maxVelocity)
					{
						rigidbody2D.velocity = rigidbody2D.velocity.normalized * Controller.maxVelocity;
					}
					else if (speed < Controller.minVelocity)
					{
						rigidbody2D.velocity = rigidbody2D.velocity.normalized * Controller.minVelocity;
					}
					Vector2 v = rigidbody2D.velocity;
					var angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
					transform.rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
				}
				else
				{
					//need some frightening running away deal
				}
			}

			float waitTime = Random.Range(0.3f, 0.5f);
			yield return new WaitForSeconds(waitTime);
		}
	}

	private Vector2 Calc()
	{
		Vector2 randomize = (new Vector2((Random.value * 2) - 1, (Random.value * 2) - 1)).normalized;
		Vector2 flockCenter = Controller.flockCenter - transform.localPosition;
		Vector2 flockVelocity = (Vector2)Controller.flockVelocity - rigidbody2D.velocity;
		Vector2 follow = Controller.chasee.transform.localPosition - transform.localPosition;

		return (flockCenter + flockVelocity + follow * 2 + randomize * Controller.randomness);
	}

	public void SetController(BoidController theController)
	{
		Controller = theController;
		inited = true;
	}
}