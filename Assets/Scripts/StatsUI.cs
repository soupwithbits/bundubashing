﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatsUI : MonoBehaviour
{
	public Image BlackOverlay;
	public Text LevelCompleteText;
	public Text[] Stats;

    // Use this for initialization
    void Start()
    {
	
    }

    public void Show()
    {
    	Stats[0].text = StatManager.instance.LitersOfBlood + " Litres of Blood Spilled";
		Stats[1].text = StatManager.instance.KGOfIvory + " Kilos of Ivory Gained";
		Stats[2].text = StatManager.instance.BoarHooves + " Boar Hooves Collected";
		Stats[3].text = StatManager.instance.ZebraSteaks + " Kilos of Zebra Steak Attained";
    	Display(true);
    }

    public void Hide()
    {
    	Display(false);
    }

    public void SetColor()
    {
        foreach (var t in Stats)
        {
            t.color = new Color(0.797f, 0.518f, 0.164f, 1.000f);
        }
        
    }

    void Display(bool d)
    {
    	BlackOverlay.enabled = d;
    	LevelCompleteText.enabled = d;
    	foreach (var t in Stats)
    	{
    		t.enabled = d;
    	}
    }
}
