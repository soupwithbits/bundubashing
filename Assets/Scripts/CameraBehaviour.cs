﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour
{
    public Transform TrackingObject;
    public float CameraDistance;

    private Camera _camera;
    private CarController _car;

    private bool _isShaking;
    private Vector3 _shakeOffset;
    private bool _shakeCooldown;
    private float _shakeX;
    private float _shakeY;
    private float _shakeTimeLeft;

    private Vector3 _offOffset;
    private float _xOffset;
    private float _yOffset;
    private float _returnSpeed;

    //Transitions
    private float _currentSizeTransitionGoal;
    private float _currentSizeTransitionSpeed;
    private Vector3 _currentVelocity;

    [HideInInspector]
    public float StartCameraSize;

    private static CameraBehaviour _instance;

    public static CameraBehaviour Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CameraBehaviour>();

            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        _camera = GetComponent<Camera>();
        _currentSizeTransitionGoal = _camera.orthographicSize;
        transform.position = TrackingObject.transform.position;
        StartCameraSize = GetComponent<Camera>().orthographicSize;
        _currentVelocity = Vector3.zero;
        _car = FindObjectOfType<CarController>();
    }
    
    void LateUpdate()
    {
        if (TrackingObject == null)
            return;

        ShakeCountdown();
        SizeTransitionStep();

        var carVelocity = (Vector3)_car.GetComponent<Rigidbody2D>().velocity;
        transform.position = TrackingObject.position + carVelocity * CameraDistance + Vector3.back * 100f;

        Shaking();
        transform.position += _shakeOffset;

        _camera.orthographicSize = UtilityFunctions.Map(Mathf.Abs(_car.CurrentSpeed), 0, _car.MaxSpeed, 500, 900);

    }

    public void Shaking()
    {

        if (_isShaking)
        {
            if (_shakeCooldown)
            {
                _shakeX = Mathf.Lerp(_shakeX, 0f, 0.1f);
                _shakeY = Mathf.Lerp(_shakeY, 0f, 0.1f);
                _isShaking = !(_shakeX <= 0.1f && _shakeY <= 0.1f);
            }
            _shakeOffset.x = Random.Range(-_shakeX, _shakeX);
            _shakeOffset.y = Random.Range(-_shakeY, _shakeY);
        }
        else
        {
            _shakeOffset = Vector3.zero;
        }

        _xOffset = Mathf.Lerp(_xOffset, 0f, _returnSpeed);
        _yOffset = Mathf.Lerp(_yOffset, 0f, _returnSpeed);

        _offOffset = new Vector3(_xOffset, _yOffset, 0f);
    }

    public void ShakeScreen(float xShake, float yShake, float shakePeriod)
    {
        _shakeX = xShake;
        _shakeY = yShake;
        _shakeTimeLeft = shakePeriod;
        _isShaking = true;
        _shakeCooldown = false;
    }

    public void ShakeScreenInc(float xShake, float yShake, float shakePeriod)
    {
        _shakeX = Mathf.Clamp(_shakeX + xShake, 0f, 15f);
        _shakeY = Mathf.Clamp(_shakeY + yShake, 0f, 15f);
        _shakeTimeLeft += shakePeriod;
        _isShaking = true;
        _shakeCooldown = false;
    }

    void ShakeCountdown()
    {
        if (_shakeTimeLeft <= 0)
        {
            _shakeCooldown = true;
        }
        else
        {
            _shakeTimeLeft -= Time.deltaTime;
        }
    }

    public void OffsetScreen(float xOffset, float yOffset, float offsetReturnSpeed)
    {
        _xOffset = xOffset;
        _yOffset = yOffset;
        _returnSpeed = offsetReturnSpeed;
    }

    //Transitions

    public void NewSizeTransitionGoal(float newGoal, float newSpeed)
    {
        _currentSizeTransitionGoal = newGoal;
        _currentSizeTransitionSpeed = newSpeed;
    }

    private void SizeTransitionStep()
    {
        float curSize = _camera.orthographicSize;
        curSize = Mathf.Lerp(curSize, _currentSizeTransitionGoal, _currentSizeTransitionSpeed);
        _camera.orthographicSize = curSize;
    }
}
