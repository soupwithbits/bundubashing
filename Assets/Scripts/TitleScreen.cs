﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using InControl;

public class TitleScreen : MonoBehaviour
{

    void Start()
    {
        SoundManager.SwitchToSafari();
        SoundManager.DMixTitle();
        GameObject.Find("HighScoreText").GetComponent<Text>().text = "HIGHSCORE: " + GetHighScore();
    }
	
    void Update()
    {
        var inputDevice = InputManager.ActiveDevice;

        if (inputDevice.Action1.IsPressed || Input.GetKeyDown(KeyCode.Return))
        {
            int level = Random.Range(2, 4);
            Application.LoadLevel(2);
        }

    }

    string GetHighScore()
    {
        return PlayerPrefs.GetInt("HighScore", 0).ToString();
    }
}
