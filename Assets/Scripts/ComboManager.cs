﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ComboManager : MonoBehaviour 
{
	public static ComboManager instance;

	private float comboTimer = 0;
	private float comboWindow = 1;
	private bool activeCombo = false;
	[HideInInspector]
	public int comboCount = 0;
	[HideInInspector]
	public int DriftCombo;
	private Text comboText;
	private Text driftKillText;
	Image vignette;

	private int ComboCount
	{
		get { return comboCount; }
		set
		{
			comboCount = value;
			if (comboCount > 1)
			{
				AkSoundEngine.PostEvent("Play_combo_hit", Camera.main.gameObject);
				comboText.fontSize = 200;
				comboText.text = "x" +  comboCount.ToString() + " combo";
				GoTweenConfig config = new GoTweenConfig();
				int fontSize = 160 + 20*comboCount;
				config.intProp("fontSize", Mathf.Clamp(fontSize, 0, 300));
				config.setEaseType(GoEaseType.BounceOut);
				Go.to(comboText, 0.5f, config);
				AlphaTweenVignette(new Color(1,1,1,1));
				SoundManager.SwitchToMetal();
			}
			else if (comboCount == 1)
			{
				comboText.text = "";
			}
			else
			{
				GoTweenConfig config = new GoTweenConfig();
				config.intProp("fontSize", 10);
				config.setEaseType(GoEaseType.QuadOut);
				config.onComplete(_ => { comboText.text = ""; StopCoroutine("SwitchOffMetal"); StartCoroutine("SwitchOffMetal"); });
				Go.to(comboText, 0.5f, config);
			}
		}
	}

	void Start () 
	{
		instance = this;
		AnimalController.OnAnimalKilled += OnAnimalKilled;
		comboText = GameObject.Find("ComboText").GetComponent<Text>();
		driftKillText = GameObject.Find("DriftKillText").GetComponent<Text>();
		vignette = GameObject.Find("vignette").GetComponent<Image>();
	}
	
	void OnDestroy()
	{
		AnimalController.OnAnimalKilled -= OnAnimalKilled;
	}

	void OnAnimalKilled(AnimalController animal)
	{
		activeCombo = true;
		comboTimer = 0;
		ComboCount++;

		if (FindObjectOfType<CarController>().IsDrifting)
		{
			DriftKill();
			DriftCombo++;
			StopCoroutine("SwitchOffMetal");
		}

		int r = Random.Range(0, 3);
		if (r == 0)
		{
			GameObject.Find("ScrollingText").GetComponent<Animator>().Play("Scroll");
		}
	}

	void DriftKill()
	{
		driftKillText.text = "DRIFT KILL!";
		driftKillText.fontSize = 200;
		Go.killAllTweensWithTarget(driftKillText.transform);
		GoTweenConfig shakeConfig = new GoTweenConfig();
		shakeConfig.shake(new Vector3(1,1,0) * 10, useLocalProperties:true);
		shakeConfig.onComplete(_ => driftKillText.text = "");
		shakeConfig.setEaseType(GoEaseType.SineInOut);
		Go.to(driftKillText.transform, 1f, shakeConfig);
	}

	void Update()
	{
		if (activeCombo)
		{
			if (comboTimer < comboWindow)
			{
				comboTimer += Time.deltaTime;
			}
			else
			{
				activeCombo = false;
				ComboCount = 0;
				DriftCombo = 0;
			}
		}
	}

	IEnumerator SwitchOffMetal()
	{
		yield return new WaitForSeconds(3f);
		SoundManager.SwitchToSafari();
		AlphaTweenVignette(new Color(1,1,1,0));
	}

	void AlphaTweenVignette(Color color)
	{
		GoTweenConfig config = new GoTweenConfig();
		config.colorProp("color", color);
		config.setEaseType(GoEaseType.SineInOut);
		Go.to(vignette, 1f, config);
	}
}