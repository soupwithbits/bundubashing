﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

    public static LevelManager instance;
    public bool GameOver = false;

    private StatsUI _statsUI;
    // Use this for initialization
    void Start()
    {
        instance = this;
        _statsUI = FindObjectOfType<StatsUI>();
    }

    void Update()
    {
        if (GameOver && Input.GetKeyDown(KeyCode.Return))
            Application.LoadLevel("TitleScreen");
			
    }
	
    public void LevelComplete()
    {
        var text = GameObject.Find("LevelCompleteText").GetComponent<Text>();
        text.text = "FLOCKS\nDESTROYED!";
        _statsUI.Show();
        GoTweenConfig config = new GoTweenConfig();
        config.intProp("fontSize", 175);
        config.setEaseType(GoEaseType.Linear);
        config.onComplete(_ => Invoke("OnLevelEndAnimationComplete", 2f));
        Go.to(text, 2f, config);
        TurnOffUI();
    }
    public void LevelFailed()
    {
        var text = GameObject.Find("LevelCompleteText").GetComponent<Text>();
        text.text = "WASTED!";

        _statsUI.Show();
		
        GoTweenConfig config = new GoTweenConfig();
        config.intProp("fontSize", 175);
        config.setEaseType(GoEaseType.Linear);
        config.onComplete(_ => Invoke("OnLevelEndAnimationComplete", 10f));
        Go.to(text, 1f, config);
        TurnOffUI();
        _statsUI.SetColor();
    }

    public void OnLevelEndAnimationComplete()
    {
        int current = PlayerPrefs.GetInt("HighScore", 0);
        if (ScoreManager.instance.Score > current)
        {
            PlayerPrefs.SetInt("HighScore", ScoreManager.instance.Score);
        }
        PlayerPrefs.Save();
        Application.LoadLevel("TitleScreen");
    }

    void TurnOffUI()
    {
        var texts = GameObject.FindObjectsOfType<Text>();
        foreach (var item in texts)
        {
            if (item.name != "LevelCompleteText" && item.name != "ScoreText")
            {
                item.color = new Color(1, 1, 1, 0);
            }
        }
        GameOver = true;
    }
}
